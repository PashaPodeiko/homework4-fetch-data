const getPost = async () => {
  const response = await fetch("https://ajax.test-danit.com/api/swapi/films")
  return response.json()
}

class filmList {
  constructor(album) {
    this.album = album
  }
  renderFilms() {
    this.album.forEach(({ characters, episodeId, name, openingCrawl }) => {
      this.div = document.createElement("div")
      this.div.innerHTML = `
		<h2>Episode ${episodeId}. ${name}</h2>
		<h3>Short content:</h3>
		<p> ${openingCrawl}</p>
		<h3>Actores:</h3>`
      document.body.append(this.div)
      this.promises = characters.map((url) => {
        return fetch(url).then((response) => response.json())
      })
      this.renderActors()
    })
  }
  renderActors() {
    const charactersUl = document.createElement("ul")
    this.div.append(charactersUl)
    Promise.all(this.promises).then((obj) => {
      obj.map(({ name }) => {
        const charactersList = document.createElement("li")
        charactersList.innerHTML = name
        charactersUl.append(charactersList)
      })
    })
  }
}

const runFunction = async () => {
  const post = await getPost()
  const render = new filmList(post)
  render.renderFilms()
}

runFunction()
